function formValidate() {
    var fields = document.querySelectorAll('.formuser__input--nickname, .formuser__input--password, .formuser__input--firstname, .formuser__input--lastname, .formuser__input--birthdate, .formuser__input--grouplistchosen');

    [].forEach.call(fields, function(element) {

        if (element.classList.contains("formuser__input--firstname")) {
            element.addEventListener('keyup', function() { TestInput(element) });
            element.addEventListener('blur', function() { TestInput(element) });
        }
        if (element.classList.contains("formuser__input--password")) {
            element.addEventListener('keyup', function() { TestInput(element) });
            element.addEventListener('blur', function() { TestInput(element) });
        }
        if (element.classList.contains("formuser__input--firstname")) {
            element.addEventListener('keyup', function() { TestInput(element) });
            element.addEventListener('blur', function() { TestInput(element) });
        }
        if (element.classList.contains("formuser__input--lastname")) {
            element.addEventListener('keyup', function() { TestInput(element) });
            element.addEventListener('blur', function() { TestInput(element) });
        }
        if (element.classList.contains("formuser__input--birthdate")) {
            element.addEventListener('keyup', function() { TestDate(element) });
            element.addEventListener('blur', function() { TestDate(element) });
        }
        if (element.classList.contains("formuser__input--grouplistchosen")) {
            element.addEventListener('keyup', function() { TestList(element) });
            element.addEventListener('blur', function() { TestList(element) });
        }

    });

}

function TestInput(input) {
    var isvalid = true;
    var pat = input.getAttribute('pattern');

    if (pat != null) {
        var reg = new RegExp(pat, 'g');

        if (!reg.test(input.value)) {
            isvalid = false;

        } else {
            if (input.value == '') {
                isvalid = false;
            }
        }
        if (isvalid) {
            showInValid(input, true);
            return true;
        } else {
            showInValid(input, false);
            return false;
        }
    }
};

function TestDate(input) {
    var isvalid = true;
    var pat = input.getAttribute('pattern');
    var picked = new Date(input.value);
    var now = new Date();

    if (pat != null) {
        var reg = new RegExp(pat, 'g');

        if (!reg.test(input.value)) {
            isvalid = false;

        }
        if (picked > now) {
            isvalid = false;
            console.log(input.value + " " + now);
        } else {
            if (input.value == '') {
                isvalid = false;
            }
        }
        if (isvalid) {
            showInValid(input, true);
            return true;
        } else {
            showInValid(input, false);
            return false;
        }
    }
}

function TestList(input) {
    var isvalid = true;
    console.log(input.nodeValue);
    if (input.nodeValue == '') {
        isvalid = false;
    }
    if (isvalid) {
        showInValid(input, true);
        return true;
    } else {
        showInValid(input, false);
        return false;
    }
}


function showInValid(input, isvalid) {
    if (!isvalid) {
        input.classList.add('invalid');
    } else {
        input.classList.remove('invalid');
    }
}