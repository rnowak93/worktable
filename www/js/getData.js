function getdata(param) {

    ajaxgetordered('http://localhost/restserv/api/getData.php', function(data) {
        var html = '<thead><tr><th ></th><th class="clickable" data-order="firstname">First name</th><th class="clickable" data-order="lastname" >Last name</th><th class="clickable" data-order="nick name">Nick name</th><th>Password</th><th class="clickable" data-order="birthdate">Birthdate</th><th class="clickable" data-order="groupname" >Group</th><th >Action</th></tr></thead><tbody>';
        let j;
        for (i = 0; i < data.length; i++) {
            j = i + 1
       
            html += '<td class="row_' + j + '">' + j + '</td>'
            html += '<td>' + data[i].firstname + '</td>';
            html += '<td>' + data[i].lastname + '</td>';
            html += '<td>' + data[i].nickname + '</td>';
            html += '<td>' +'**********'+'</td>';
            html += '<td>' + data[i].birthdate + '</td>';
            html += '<td>' + data[i].name + '</td>'
            html += '<td><button type="button" class="btn--userchange btn btn-info" data-toggle="modal" data-userid="' + data[i].user_id + '" data-target=".modal--user">Edit</button><button type="button" class="btn--userdel btn btn-danger" data-userid="' + data[i].user_id + '" >Delete</button></td>';
            html += '</tr>';
        }
        html += '</tbody>';
        document.getElementById('content').innerHTML = html;



        $('.btn--userdel').each(function() {
            this.addEventListener('click', function() {
                if (window.confirm("Are you sure you want to delete this user?")) {
                    userDel(this.getAttribute("data-userid"));
                }
            });
        });
        $('.btn--userchange').each(function() {
            this.addEventListener('click', function() {
                get_editUser(this.getAttribute("data-userid"));
                unbindData();

                var text= document.querySelector(".modal__header--user"),
                            btntext = document.querySelector(".form__btn--submituser");
                            text.innerHTML ="Edit user";
                            btntext.innerHTML ="Edit user";
            });
        });
        $(".clickable").each(function() {
            this.addEventListener('click', function() {
                getdata(this.getAttribute("data-order"));

            });
        });

    }, param);
    getGroupList();

}

function getGroupList() {
    var selectinput = document.querySelector('.formuser__input--grouplist');
    for (var i = selectinput.options.length - 1; i >= 0; i--){
        selectinput.remove(i);
};
    ajaxget('http://localhost/restserv/api/getGroupsList.php', function(data) {
        for (i = 0; i < data.length; i++)
            $('<option  data-groupid="' + data[i].group_id + '" value="' + data[i].group_id +  '">' + data[i].name + '</option>').appendTo(selectinput);
            
    });

};

function getGroupData(param) {
    var html1 = '<thead><tr><th></th><th class="clickable" data-order="groupname">Group name</th><th <th class="clickable" data-order="mnumber">Number of members</th><th width="200px">Action</th></tr></thead><tbody>';
    var j;

    ajaxgetordered('http://localhost/restserv/api/getGroups.php', function(data) {

        for (i = 0; i < data.length; i++) {
            j = i + 1;
            if (data[i].group_id == "0") {
                html1 += '<td>' + j + '</td>';
                html1 += '<td>' + data[i].name + '</td>';
                html1 += '<td>' + data[i].mnumber + '</td>';
                html1 += '<td></td>';
                html1 += '</tr>';
            } else {
                html1 += '<td>' + j + '</td>';
                html1 += '<td>' + data[i].name + '</td>';
                html1 += '<td>' + data[i].mnumber + '</td>';
                html1 += '<td><button type="button" class="btn--groupchange btn btn-info" data-toggle="modal"  data-groupid="' + data[i].group_id + '" data-target=".modal--group">Edit</button><button type="button" class="btn--groupdel btn btn-danger" data-toggle="modal"  data-groupid="' + data[i].group_id + '" >Delete</button></td>';
                html1 += '</tr>';

            }
        }
        loadElseGroups(html1, j);
    }, param);

}

function loadElseGroups(html1, j) {

    ajaxget('http://localhost/restserv/api/getEmptyGroups.php', function(data) {

        for (i = 0; i < data.length; i++) {
            j = j + 1;
            if (data[i].group_id == "0") {
                html1 += '<td>' + j + '</td>';
                html1 += '<td>' + data[i].name + '</td>';
                html1 += '<td>0</td>';
                html1 += '<td></td>';
                html1 += '</tr>';
            } else {
                html1 += '<td>' + j + '</td>';
                html1 += '<td>' + data[i].name + '</td>';
                html1 += '<td>0</td>';
                html1 += '<td><button type="button" class="btn--groupchange btn btn-info" data-toggle="modal" data-groupid="' + data[i].group_id + '" data-target=".modal--group">Edit</button><button type="button" class="btn--groupdel btn btn-danger" data-toggle="modal"  data-groupid="' + data[i].group_id + '" >Delete</button></td>';
                html1 += '</tr>';
            }
        }
        html1 += '</tbody>';
        document.getElementById('content').innerHTML = html1;


        $('.btn--groupdel').each(function() {
            this.addEventListener('click', function() {
                if (window.confirm("Are you sure you want to delete this group?")) {
                    groupDel(this.getAttribute("data-groupid"));
                }
            });
        });
        $('.btn--groupchange').each(function() {
            this.addEventListener('click', function() {
                get_editGroup(this.getAttribute("data-groupid"));
                 var text1 = document.querySelector(".modal__header--group"),
                    btntext1 = document.querySelector(".form__btn--submitgroup");
                    text1.innerHTML ="Edit group";
                    btntext1.innerHTML ="Edit group";

            });
        });
        $(".clickable").each(function() {
            this.addEventListener('click', function() {
                getGroupData(this.getAttribute("data-order"));

            });
        });

    });

}

function ajaxgetordered(url, callback, param) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // console.log('responseText:' + xhr.responseText);
            try {
                var data = JSON.parse(xhr.responseText);
            } catch (err) {
                console.log(err.message + " in " + xhr.responseText);
                return;
            }
            callback(data);
        }
    };
    xhr.open("GET", url + "?param=" + param, true);
    xhr.send();
};

function ajaxget(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // console.log('responseText:' + xhr.responseText);
            try {
                var data = JSON.parse(xhr.responseText);
            } catch (err) {
                console.log(err.message + " in " + xhr.responseText);
                return;
            }
            callback(data);
        }
    };
    xhr.open("GET", url, true);
    xhr.send();
};
//Get data edited user
function get_editUser(userid) {
    var user_id = "user_id=" + userid;
    $('.formuser').attr("data-edit", "1");

    $.ajax({
        type: "POST",
        url: "http://localhost/restserv/api/getUser.php",
        data: user_id,
        success: function(response) {
            try {
                var jsonresponse = JSON.parse(response);
            } catch (err) {
                console.log(err.message + " in " + response);
                return;
            }
            console.log(jsonresponse);
            bindData(jsonresponse, userid);
        },
        error: function(xhr) {
            console.log('blad' + xhr.status);
        }

    });

}
//Get a change group name
function get_editGroup(groupid) {
    var group_id = "group_id=" + groupid;
    $('.formgroup').attr("data-edit", "1");

    $.ajax({
        type: "POST",
        url: "http://localhost/restserv/api/getGroup.php",
        data: group_id,
        success: function(response) {
            try {
                var jsonresponse = JSON.parse(response);

            } catch (err) {
                console.log(err.message + " in " + response);
                return;
            }
            bindGroup(jsonresponse, groupid);
        },
        error: function(xhr) {
            console.log('blad' + xhr.status);
        }

    });

}