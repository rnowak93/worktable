function userDel(userid) {
    var user_id = "user_id=" + userid + "";

    $.ajax({
        type: "POST",
        url: "http://localhost/restserv/api/delUser.php",
        data: user_id,
        success: function(data) {
            var response = JSON.parse(data);
            switch (response.status) {
                case "userdeleted":
                    alert("SUCCESS! User has been deleted.");
                    break;
                case "NINT":
                    alert("ERROR! Wrong ID of user.");
                    break;
                case "userdelgroupempty":
                    alert("SUCCESS! User has been deleted, group is empty now.");
                    break;
                case "empty":
                    alert("ERROR! Empty ID of user.");
                    break;
                default:
                    console.log(response);

            };
            getdata();
        },
        error: function() {
            console.log("Connection error.");
        }


    });
}

function groupDel(groupid) {
    var group_id = "group_id=" + groupid + "";
    console.log('gropdel function check' + group_id);
    $.ajax({
        type: "POST",
        url: "http://localhost/restserv/api/delGroup.php",
        data: group_id,
        success: function(data) {
            var response = JSON.parse(data);
            switch (response.status) {
                case "emptydeleted":
                    alert("SUCCESS! Group has been deleted.");
                    break;
                case "error":
                    alert("ERROR! Wrong ID of group.");
                    break;
                case "usersmoved":
                    alert("SUCCESS! Group has been deleted, users moved to: No group affiliation.");
                    break;
                case "empty":
                    alert("ERROR! Empty ID of group.");
                    break;
                default:
                    console.log(response);

            };
            getGroupData();
        },
        error: function() {
            console.log("Connection error.");
        }

    });
}