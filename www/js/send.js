function formSubmitValidate() {
    let validated = true;
    var fields = document.querySelectorAll('.formuser__input--nickname, .formuser__input--password, .formuser__input--firstname, .formuser__input--lastname, .formuser__input--birthdate, .formuser__input--grouplistchosen');
    [].forEach.call(fields, function(element) {

        if (element.classList.contains('formuser__input--nickname')) {
            if (!TestInput(element)) validated = false;
        }
        if (element.classList.contains('formuser__input--password')) {
            if (!TestInput(element)) validated = false;

        }
        if (element.classList.contains('formuser__input--firstname')) {
            if (!TestInput(element)) validated = false;

        }
        if (element.classList.contains('formuser__input--lastname')) {
            if (!TestInput(element)) validated = false;
        }
        if (element.classList.contains('formuser__input--birthdate')) {
            if (!TestDate(element)) validated = false;
        }
    });

    if (validated) {
        groupid = new Array ();
        if(document.querySelector('.formuser__input--nogroup').checked){
            groupid = ["0"];
        }else{
            var select = document.querySelector('.formuser__input--grouplistchosen');
             if (select.length>0) {
                for(i=select.options.length-1;i>=0;i--){
                    groupid[i] = select.options[i].value;
        }
        console.log(groupid);
    }
        }


        this.sendUser(fields, groupid);

    } else {
        console.log(fields)
        alert("błąd");
        return false;
    }
}

function sendUser(fields, groupid) {
    var userdata ={"nickname":fields[0].value,"password":fields[1].value,"firstname":fields[2].value,"lastname":fields[3].value,"birthdate":fields[4].value,"group_id":groupid};
    var status = document.querySelector('.formuser').getAttribute("data-edit");
        var jsonUserDataString = JSON.stringify(userdata);

    if (status == "0") {
        $.ajax({
            type: "POST",
            url: "http://localhost/restserv/api/addUserj.php",
            data: {data:jsonUserDataString},
            success: function(data) {
                console.log(data);
                var response = JSON.parse(data);
                console.log(response);
                switch (response.status) {
                    case "exist":
                        alert("ERROR! User on the same name or password allready exist.");
                        break;
                    case "empty":
                        alert("ERROR! No user id.");
                        break;
                    case "wrongchars":
                        alert("ERROR! Data does not meet requirements.");
                        break;
                    case "ok":
                        alert("SUCCESS! User has been added.");
                        $('.modal--user').modal('hide');
                        getdata();
                        unbindData();
                        break;
                    default:
                        console.log(response);

                };

            },
            error: function() {
                console.log("Connection error.");
            }
        });
    }
    if (status == "1") {
        var who = document.querySelector('.formuser').getAttribute("data-userid");
        var dataObiect ={"user_id":who,"nickname":fields[0].value,"password":fields[1].value,"firstname":fields[2].value,"lastname":fields[3].value,"birthdate":fields[4].value,"group_id":groupid};
        var jsonDataString = JSON.stringify(dataObiect);
        $.ajax({
            type: "POST",
            url: "http://localhost/restserv/api/editUserj.php",
            data:{data : jsonDataString}, 
            success: function(data) {
            var response = JSON.parse(data);
                

                switch (response.status) {
                    case "exist":
                        alert("ERROR! User on the same name or password allready exist.");
                        break;
                    case "empty":
                        alert("ERROR! No user id.");
                        break;
                    case "wrongchars":
                        alert("ERROR! Data does not meet requirements.");
                        break;
                    case "ok":
                        alert("SUCCESS! User has been edited.");
                        $('.modal--user').modal('hide');
                        getdata();
                        unbindData();
                        break;
                    default:
                        console.log(response);

                };
                $('#user-modal-form').modal('hide');
                getdata();
                unbindData();
            },
            error: function() {
                console.log("Connection error.");
            }



        });
    }
}
//###############################GROUPS###############################
function groupformSubmit() {
    var field = document.querySelector('.formgroup__input--groupname');
    let validated = true;

    if (!TestInput(field)) validated = false;

    if (validated) {
        this.sendGroup(field);
        //  console.log('2'+groupid);

    } else {
        console.log(field);
        alert("błąd");
        return false;
    }

}

function sendGroup(field) {
    var data = "groupname=" + field.value;
 
    var status = document.querySelector('.formgroup').getAttribute("data-edit");
    var who = document.querySelector('.formgroup').getAttribute("value");//value
    if (status == "0") {
        $.ajax({
            type: "POST",
            url: "http://localhost/restserv/api/addGroup.php",
            data: data,
            success: function(data) {
                var response = JSON.parse(data);
                switch (response.status) {
                    case "exist":
                        alert("ERROR! Group on the same name allready exist.");
                        break;
                    case "noname":
                        alert("ERROR! No group name.");
                        break;
                    case "wrongchars":
                        alert("ERROR! First letter capital, numbers without special like: ^<>|# .");
                        break;
                    case "ok":
                        alert("SUCCESS! New group added.");
                        $('.modal--group').modal('hide');
                        getGroupData()
                        unbindGroup();
                        break;
                    default:
                        console.log(response);

                };

            },
            error: function() {
                console.log("Connection error.");
            }

        });
    }
    if (status == "1") {
        var group_id = "group_id=" + who;

        $.ajax({
            type: "POST",
            url: "http://localhost/restserv/api/editGroup.php",
            data: group_id + '&' + data,
            success: function(data) {
                var response = JSON.parse(data);
                switch (response.status) {
                    case "exist":
                        alert("ERROR! Group on the same name allready exist.");
                        break;
                    case "noname":
                        alert("ERROR! No group name.");
                        break;
                    case "wrongchars":
                        alert("ERROR! First letter capital, numbers without special like: ^<>|# OR  You are trying to rename a base group 'No affiliation group' which is not allowed.");
                        break;
                    case "ok":
                        alert("SUCCESS! Group edited.");
                        $('.modal--group').modal('hide');
                        getGroupData()
                        unbindGroup();
                        break;
                    default:
                        console.log(response);
                };

            },
            error: function() {
                console.log("Connection error.");
            }
        });
    }




}