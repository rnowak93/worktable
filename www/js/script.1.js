document.addEventListener("DOMContentLoaded", function(event) {
    var param = "firstname";
    getdata(param);
    formValidate();

    var switchu = document.querySelector('.btn--switchuser');
    var switchg = document.querySelector('.btn--switchgroup');

    switchu.addEventListener("click", function() {
        getdata();
    });

    switchg.addEventListener("click", function() {
        getGroupData();
    });
    document.querySelector(".formuser").addEventListener('submit', function(e) {
        e.preventDefault();
        formSubmitValidate();
    });

    document.querySelector(".formgroup").addEventListener('submit', function(e) {
        e.preventDefault();
        groupformSubmit();
    });

var checkbox = document.querySelector(".formuser__input--nogroup");
var listarea = document.querySelector(".grouplistshow");
         checkbox.addEventListener("click", function(){
             if(this.checked){
                 listarea.style.display = "none";
             }else
                 listarea.style.display = "flex";

    
 
 
    document.querySelector('.btn--createuser').addEventListener('click', function() {
        $(".formuser").attr("data-edit", "0");
        unbindData();


        var text = document.querySelector(".modal__header--user"),
            btntext = document.querySelector(".form__btn--submituser");
            text.innerHTML ="Create user";
            btntext.innerHTML ="Create user";

    });

    var field = document.querySelector('.formgroup__input--groupname');
    field.addEventListener('keyup', function() { TestInput(field) });
    field.addEventListener('blur', function() { TestInput(field) });

    document.querySelector(".btn--creategroup").addEventListener('click', function() {
        $('.form--group').attr("data-edit", "0");
        unbindGroup();
        var text1 = document.querySelector(".modal__header--group"),
            btntext1 = document.querySelector(".form__btn--submitgroup");
            text1.innerHTML ="Create group";
            btntext1.innerHTML ="Create group";
            
    });
    
});
    document.querySelector('.add').addEventListener('click', function(){
        moveOptions('.formuser__input--grouplist', '.formuser__input--grouplistchosen');
    });
        document.querySelector('.rem').addEventListener('click', function(){
        moveOptions('.formuser__input--grouplistchosen','.formuser__input--grouplist')
    });



    
});


// Bind picked data from AJAX query to form inputs.
function bindData(data, userid) {

    var inputs = document.querySelectorAll('.formuser__input--nickname, .formuser__input--password, .formuser__input--firstname, .formuser__input--lastname, .formuser__input--birthdate');
    $('.formuser').attr("data-userid", userid);

    if(data[0].group_id==0){
         document.querySelector(".formuser__input--nogroup").checked = false;
    }else{
        document.querySelector(".formuser__input--nogroup").checked = true;
      var list = document.querySelector('.formuser__input--grouplist').children;
      var grouplistinput = document.querySelector(".formuser__input--grouplist");
      let i;
      for(i=0;i<data.length;i++){
          for(j=0;j<list.length;j++){
            if(grouplistinput.options[j].getAttribute("value")==data[i].group_id){
            grouplistinput.options[j].selected = true;
          }

        }
              moveOptions(".formuser__input--grouplist",".formuser__input--grouplistchosen");
      }
    }
    inputs[0].value = data[0].nickname;
    inputs[1].value = data[0].password;
    inputs[2].value = data[0].firstname;
    inputs[3].value = data[0].lastname;
    inputs[4].value = data[0].birthdate;
    // inputs[5].value = data[0].name;

    formValidate();
     document.querySelector(".formuser__input--nogroup").click();

}


function unbindData() {

    var inputs = document.getElementsByClassName("formuser__input");

    for (let i = 0; i < inputs.length - 1; i++) {
        inputs[i].value = "";
        inputs[i].classList.remove('invalid');
    };
   let select = document.querySelector(".formuser__input--grouplistchosen");
    if (select.length>0) {
        for(i=select.options.length-1;i>=0;i--){
         select.options[i].selected = true;
      moveOptions(".formuser__input--grouplistchosen",".formuser__input--grouplist");
        }
    }
    // inputs[5].value = "No group affiliation";
         document.querySelector(".formuser__input--nogroup").checked = false;
        document.querySelector(".formuser__input--nogroup").click();

};

function bindGroup(data, groupid) {
    $('.formgroup').attr("data-groupid", groupid);

    var input = document.querySelector(".formgroup__input--groupname");
    input.value = data[0].name;
}

function unbindGroup() {
    var input = document.querySelector(".formgroup__input--groupname");
    input.value = "";
    input.classList.remove('invalid');



}
function moveOptions(selectFrom, selectTo) {
    let select1 = document.querySelector(selectFrom);
    let select2 = document.querySelector(selectTo);

    if (select1.length>0) {
        for (let x=0; x<select1.length; x++){
            if (select1.options[x].selected) {
                var newOption = select1.options[x].cloneNode(true);
                select2[select2.length] = newOption;
                select2.appendChild(newOption);
                select1.removeChild(select1.options[x]);
            }
        }
    }
}

