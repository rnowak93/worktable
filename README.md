# Project readme. 

This project contains a database and a client.
The client has a basic functionalities like display, create, edit, delete, content of database.


### Technologies
* HTML 5
* CSS
* JAVASCRIPT
* BOOTSTRAP
* jQuery
* AJAX
* Gulp
* MYSQL
* PHP

## Main page


![](https://github.com/nowyxx/worktable/blob/master/resorces/mainpage.png)

Main page contains information about stored users in database and has all functionalities associated with users like edit user or delete user from database.
Clicking on a column name will display the list in order.
This page also allow to add new user or group to database by click on "Create user"/"Create group" button. 
## Edit user form

![](https://github.com/nowyxx/worktable/blob/master/resorces/edituser.png)

User edition form is the same as create users form, but these forms has some rules concerning to data of input.

Validate rules:
* nick name - letters, numbers without special like: ^<>|#
* password - password must contain at least 5 characters
* first name - first letter capital, 2-35 characters
* last name - the same rules as firstname
* birth date - YYYY-MM-DD format, date in past
* group - group from list

##  Group page

![](https://github.com/nowyxx/worktable/blob/master/resorces/grouppage.png)

Groups page contains information about stored groups in database and has all functionalities like main users page but, associated with groups.

##  Edit group form
![](https://github.com/nowyxx/worktable/blob/master/resorces/editgroup.png)

Group edit form is a little bit different than users form contain only group name. Group name must start by capital character and could contain letters number under 25 character. 
